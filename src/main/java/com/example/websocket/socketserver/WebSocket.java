package com.example.websocket.socketserver;

import com.alibaba.fastjson.JSON;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @Author: yang_li
 * @Date: 2024/4/1117:25
 */
@Slf4j

@Component
@ServerEndpoint("/websocketOne/{username}")
@EqualsAndHashCode
public class WebSocket {

    /**
     * 实例一个session，这个session是websocket的session
     */
    private Session session;

    /**
     * 存放用户名
     */
    private String userName;

    /**
     * 存放需要接受消息的用户名
     */
    private String toUserName;

    /**
     * 存放在线用户的数量
     */
    private static Integer USER_NUMBER = 0;

    /**
     * 存放websocket的集合
     */
    private static final CopyOnWriteArraySet<WebSocket> WEB_SOCKET_SET = new CopyOnWriteArraySet<>();

    /**
     * 前端请求时一个websocket时
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("username") String userName) {
        this.session = session;
        //将当前对象放入webSocketSet
        WEB_SOCKET_SET.add(this);
        //增加在线人数
        synchronized (this){
            USER_NUMBER++;
        }
        //保存当前用户名
        this.userName = userName;
        //获得所有的用户
        Set<String> userLists = new TreeSet<>();
        for (WebSocket webSocket : WEB_SOCKET_SET) {
            userLists.add(webSocket.userName);
        }

        //将所有信息包装好传到客户端(给所有用户)
        HashMap<String, Object> map = new HashMap<>();
        //  把所有用户列表
        map.put("onlineUsers", userLists);
        //messageType 1代表上线 2代表下线 3代表在线名单 4代表普通消息
        map.put("messageType", 1);
        //  返回用户名
        map.put("username", userName);
        //  返回在线人数
        map.put("number", USER_NUMBER);
        //发送给所有用户谁上线了，并让他们更新自己的用户菜单
        sendMessageAll(JSON.toJSONString(map),this.userName);
        log.info("【websocket消息】有新的连接, 总数:{}", USER_NUMBER);

        // 更新在线人数(给所有人)
        HashMap<String, Object> map2 = new HashMap<>();
        //messageType 1代表上线 2代表下线 3代表在线名单 4代表普通消息
        map2.put("messageType", 3);
        //把所有用户放入map2
        map2.put("onlineUsers", userLists);
        //返回在线人数
        map2.put("number", USER_NUMBER);
        // 消息发送指定人（所有的在线用户信息）
        sendMessageAll(JSON.toJSONString(map2),this.userName);

    }

    /**
     * 前端关闭时一个websocket时
     */
    @OnClose
    public void onClose() {
        //从集合中移除当前对象
        WEB_SOCKET_SET.remove(this);
        //在线用户数减少
        synchronized (this){
            USER_NUMBER--;
        }
        HashMap<String, Object> map1 = new HashMap<>();
        //messageType 1代表上线 2代表下线 3代表在线名单 4代表普通消息
        map1.put("messageType", 2);
        //所有在线用户
        map1.put("onlineUsers", WEB_SOCKET_SET);
        //下线用户的用户名
        map1.put("username", this.userName);
        //返回在线人数
        map1.put("number", USER_NUMBER);
        //发送信息，所有人，通知谁下线了
        sendMessageAll(JSON.toJSONString(map1),this.userName);
        //通知修改用户列表
        // 更新在线人数(给所有人)
        Map<String, Object> map2 = new HashMap();
        //获得所有的用户
        Set<String> userLists = new TreeSet<>();
        for (WebSocket webSocket : WEB_SOCKET_SET) {
            userLists.add(webSocket.userName);
        }
        //messageType 1代表上线 2代表下线 3代表在线名单 4代表普通消息
        map2.put("messageType", 3);
        //把所有用户放入map2
        map2.put("onlineUsers", userLists);
        //返回在线人数
        map2.put("number", USER_NUMBER);
        // 消息发送指定人（所有的在线用户信息）
        sendMessageAll(JSON.toJSONString(map2),this.userName);
        log.info("【websocket消息】连接断开, 总数:{}", WEB_SOCKET_SET.size());
    }

    /**
     * 前端向后端发送消息
     *
     * @param message `
     */
    @OnMessage
    public void onMessage(String message) {
        log.info("【websocket消息】收到客户端发来的消息:{}", message);
        //将前端传来的数据进行转型
        com.alibaba.fastjson.JSONObject jsonObject = JSON.parseObject(message);
        //获取所有数据
        String textMessage = jsonObject.getString("message");
        String username = jsonObject.getString("username");
        String type = jsonObject.getString("type");
        String tousername = jsonObject.getString("tousername");
        //群发
        if("all".equals(type)){
            HashMap<String, Object> map3 = new HashMap<>();
            map3.put("messageType", 4);
            //所有在线用户
            map3.put("onlineUsers", WEB_SOCKET_SET);
            //发送消息的用户名
            map3.put("username", username);
            //返回在线人数
            map3.put("number", USER_NUMBER);
            //发送的消息
            map3.put("textMessage", textMessage);
            //发送信息，所有人，通知谁下线了
            sendMessageAll(JSON.toJSONString(map3),this.userName);
        }
        //私发
        else{
            //发送给对应的私聊用户
            HashMap<String, Object> map3 = new HashMap<>();
            map3.put("messageType", 4);
            //所有在线用户
            map3.put("onlineUsers", WEB_SOCKET_SET);
            //发送消息的用户名
            map3.put("username", username);
            //返回在线人数
            map3.put("number", USER_NUMBER);
            //发送的消息
            map3.put("textMessage", textMessage);
            //发送信息，所有人，通知谁下线了
            sendMessageTo(JSON.toJSONString(map3),tousername);

            //发送给自己
            HashMap<String, Object> map4 = new HashMap<>();
            map4.put("messageType", 4);
            //所有在线用户
            map4.put("onlineUsers", WEB_SOCKET_SET);
            //发送消息的用户名
            map4.put("username", username);
            //返回在线人数
            map4.put("number", USER_NUMBER);
            //发送的消息
            map4.put("textMessage", textMessage);
            //发送信息，所有人，通知谁下线了
            sendMessageTo(JSON.toJSONString(map4),username);
        }
    }

    /**
     * 新增一个方法用于主动向客户端发送消息
     *
     * @param message `
     */
    public static void sendMessage(String message) {
        for (WebSocket webSocket : WEB_SOCKET_SET) {
            log.info("【websocket消息】广播消息, message={}", message);
            try {
                webSocket.session.getBasicRemote().sendText(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessageAll(String message, String fromUserName) {
        for (WebSocket webSocket : WEB_SOCKET_SET) {
            try {
                webSocket.session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessageTo(String message, String toUserName) {
        for (WebSocket webSocket : WEB_SOCKET_SET) {
            if (webSocket.userName.equals(toUserName)) {
                try {
                    webSocket.session.getBasicRemote().sendText(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
