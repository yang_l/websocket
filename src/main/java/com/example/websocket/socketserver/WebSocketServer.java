package com.example.websocket.socketserver;

import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.*;

import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author yang_li
 */
@Slf4j
@ServerEndpoint("/websocket")
@EqualsAndHashCode
public class WebSocketServer implements WebSocketHandler {


    /**
     * 实例一个session，这个session是websocket的session
     */
    private WebSocketSession session;

    private String userName;

    private static Integer userNumber;

    /**
     * 存放websocket的集合
     */
    private static final CopyOnWriteArraySet<WebSocketServer> WEB_SOCKET_SET = new CopyOnWriteArraySet<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        log.info("链接建立成功");
        this.session = session;
        WEB_SOCKET_SET.add(this);
        log.info("【websocket消息】有新的连接, 总数:{}", WEB_SOCKET_SET.size());
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) {
        log.info("收到消息，{}", message.getPayload());
        try {
            session.sendMessage(new TextMessage("hello too"));
            session.sendMessage(new BinaryMessage("hello world".getBytes()));
            log.info("【websocket消息】收到客户端发来的消息:{}", message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
        log.info("发生异常");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) {
        log.info("链接断开");
        WEB_SOCKET_SET.remove(this);
        log.info("【websocket消息】连接断开, 总数:{}", WEB_SOCKET_SET.size());
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
    /**
     * 新增一个方法用于主动向客户端发送消息
     *
     * @param message ·
     */
    public static void sendMessage(String message) {
        for (WebSocketServer webSocket: WEB_SOCKET_SET) {
            log.info("【websocket消息】广播消息, message={}", message);
            try {
                webSocket.session.sendMessage(new TextMessage(message));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessageAll(String message, String fromUserName){
        for (WebSocketServer webSocket : WEB_SOCKET_SET){
            try {
                webSocket.session.sendMessage(new TextMessage(message));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessageTo(String message, String toUserName){
        for (WebSocketServer webSocket : WEB_SOCKET_SET){
            if (webSocket.userName.equals(toUserName)){
                try {
                    webSocket.session.sendMessage(new TextMessage(message));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}