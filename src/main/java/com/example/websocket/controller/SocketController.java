package com.example.websocket.controller;

import com.example.websocket.socketserver.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Author: yang_li
 * @Date: 2024/4/214:56
 */

@Slf4j
@RestController
@RequestMapping
public class SocketController {

    @PostMapping("jinDuTiao")
    public void jinDuTiao() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            Thread.sleep(30);
            WebSocketServer.sendMessage(String.valueOf((i+1)));
        }
    }
}
